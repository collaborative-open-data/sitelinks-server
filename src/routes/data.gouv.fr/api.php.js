const entriesRegexps = [
  /^(datasets)\/([-0-9A-Za-z]+)$/,
  /^(organizations)\/([-0-9A-Za-z]+)$/,
  /^(resources)\/([-0-9a-f]+)$/,
  /^(users)\/([-0-9A-Za-z]+)$/,
]

export async function get(req, res) {
  const { action } = req.query
  if (action === "query") {
    // Cf https://www.mediawiki.org/wiki/API:Query

    const { /* converttitles, */ format, prop, /* redirects, */ titles } = req.query
    if (format === "json" && prop === "info") {
      // Cf https://www.mediawiki.org/w/api.php?action=help&modules=query%2Binfo

      if (!titles) {
        titles = ""
      }
      let entry = null
      for (let regexp of entriesRegexps) {
        const match = regexp.exec(titles)
        if (match !== null) {
          entry = {
            kind: match[1],
            slug: match[2],
          }
          break
        }
      }
      if (entry === null) {
        res.writeHead(200, {
          "Content-Type": "application/json",
        })
        return res.end(
          JSON.stringify(
            {
              batchcomplete: "",
              query: {
                pages: {
                  "-1": {
                    ns: 0, // namespace ID
                    title: titles,
                    missing: "", // page not found
                    contentmodel: "json",
                    pagelanguage: "fr",
                    pagelanguagehtmlcode: "fr",
                    pagelanguagedir: "ltr",
                  },
                },
              },
            },
            null,
            2
          )
        )
      }
      const query = {
        pages: {
          [entry.slug]: {
            pageid: entry.slug, // Should be a number.
            ns: 0, // namespace 0 (default) for entry
            title: `${entry.kind}/${entry.slug}`,
            contentmodel: "json",
            pagelanguage: "fr",
            pagelanguagehtmlcode: "fr",
            pagelanguagedir: "ltr",
            // touched  "2018-10-24T20:45:29Z"
            // lastrevid  153345869
            // length  56564
          },
        },
      }
      res.writeHead(200, {
        "Content-Type": "application/json",
      })
      return res.end(
        JSON.stringify(
          {
            batchcomplete: "",
            query,
          },
          null,
          2
        )
      )
    } else {
      res.writeHead(400, {
        "Content-Type": "application/json",
      })
      res.end(
        JSON.stringify(
          {
            error: {
              code: 400,
              message: `Unexpected options for action: ${action}.`,
            },
          },
          null,
          2
        )
      )
    }
  } else {
    res.writeHead(400, {
      "Content-Type": "application/json",
    })
    res.end(
      JSON.stringify(
        {
          error: {
            code: 400,
            message: `Unexpected action: ${action}.`,
          },
        },
        null,
        2
      )
    )
  }
}
