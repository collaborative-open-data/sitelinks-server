import sirv from "sirv"
import polka from "polka"
import compression from "compression"
import * as sapper from "../__sapper__/server"

const { PORT, NODE_ENV } = process.env
const dev = NODE_ENV === "development"

const app = polka()
if (dev) {
  // Add logging.
  const morgan = require("morgan")
  app.use(morgan("dev"))
}
app
  .use(
    compression({ threshold: 0 }),
    sirv("static", { dev }),
    sapper.middleware()
  )
  .listen(PORT, err => {
    if (err) console.log("error", err)
  })
