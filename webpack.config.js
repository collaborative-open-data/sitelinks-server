const webpack = require("webpack")
const config = require("sapper/config/webpack.js")

const pkg = require("./package.json")

const clientEntry = config.client.entry()
clientEntry.main = ["@babel/polyfill", clientEntry.main]
const serverEntry = config.server.entry()
serverEntry.server = ["@babel/polyfill", serverEntry.server]

const mode = process.env.NODE_ENV || "production"
const dev = mode === "development"

module.exports = {
  client: {
    entry: clientEntry,
    output: config.client.output(),
    resolve: {
      extensions: [".js", ".json", ".html"],
      mainFields: ["svelte", "module", "browser", "main"],
    },
    module: {
      rules: [
        {
          test: /\.html$/,
          use: [
            "babel-loader",
						{
							loader: "svelte-loader",
							options: {
								dev,
								hydratable: true,
								hotReload: true,
							},
						},
            {
              loader: "eslint-loader",
              options: {
                emitWarning: dev,
                fix: true,
              },
            },
					],
        },
      ],
    },
    mode,
    plugins: [
      dev && new webpack.HotModuleReplacementPlugin(),
      new webpack.DefinePlugin({
        "process.browser": true,
        "process.env.NODE_ENV": JSON.stringify(mode),
      }),
    ].filter(Boolean),
    devtool: dev && "inline-source-map",
  },

  server: {
    entry: serverEntry,
    output: config.server.output(),
    target: "node",
    resolve: {
      extensions: [".js", ".json", ".html"],
      mainFields: ["svelte", "module", "browser", "main"],
    },
    externals: Object.keys(pkg.dependencies).concat("encoding"),
    module: {
      rules: [
        {
          test: /\.html$/,
          use: {
            loader: "svelte-loader",
            options: {
              css: false,
              generate: "ssr",
              dev,
            },
          },
        },
      ],
    },
    mode,
    performance: {
      hints: false, // it doesn't matter if server.js is large
    },
  },
}
