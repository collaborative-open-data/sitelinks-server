# SiteLinks-Server

_Web service providing a MediaWiki-compatible API for sitelinks_

## Installation & Configuration

```bash
git clone https://framagit.org/collaborative-open-data/sitelinks-server.git
cd sitelinks-server/
npm install
```

## Running Server...

### ... in development mode

```bash
npm run dev
```

### ... in production mode

```bash
npm run build
```
